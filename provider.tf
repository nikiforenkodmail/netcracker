terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "domains" {}

variable "do_token" {}

variable "ssh_keys" {}

variable "images" {}

variable "tags" {}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_droplet" "mydroplets" {
  image = "${element(var.images, count.index)}"  
  name = "${element(var.domains, count.index)}"              
  region = "nyc1"
  size = "s-1vcpu-1gb"
  count = "${length(var.domains)}"                              
  ssh_keys = [data.digitalocean_ssh_key.ssh_key.id]
}

resource "null_resource" "connect" {
  count = "${length(var.domains)}"
  provisioner "remote-exec" {
  connection {
    host = "${digitalocean_droplet.mydroplets[count.index].ipv4_address}"
    user = "root"
    private_key = "${file("~/.ssh/id_rsa")}"
  }

  inline = ["echo 'connected!'"]
  }

  provisioner "local-exec" {
    command = "ansible-playbook --user root --private-key ~/.ssh/id_rsa -i '${digitalocean_droplet.mydroplets[count.index].ipv4_address},' prod.yml -t \"${element(var.tags, count.index)}\""
  }
}

resource "digitalocean_domain" "mydomain" {
  name = "nsxnet.ga"
}

resource "digitalocean_record" "for_mydroplets" {
  count  = "${length(var.domains)}"
  domain = digitalocean_domain.mydomain.name
  type   = "A"
  name   = "${element(var.domains, count.index)}"
  value  = digitalocean_droplet.mydroplets[count.index].ipv4_address
}

data "digitalocean_ssh_key" "ssh_key" {
  name = "${var.ssh_keys}"
}
